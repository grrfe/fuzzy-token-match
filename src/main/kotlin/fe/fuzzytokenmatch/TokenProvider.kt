package fe.fuzzytokenmatch

import fe.fuzzytokenmatch.FuzzyTokenMatcher.Companion.DEFAULT_EXACT_MATCH_MULTIPLIER
import fe.fuzzytokenmatch.FuzzyTokenMatcher.Companion.DEFAULT_PERFECT_RATIO_MULTIPLIER
import me.xdrop.fuzzywuzzy.Applicable

interface TokenProvider {
    fun provideTokens(): List<Token>
}

abstract class LazyTokenProvider : TokenProvider {
    protected var cachedTokens: List<Token>? = null

    abstract fun lazyProvideTokens(): List<Token>

    override fun provideTokens(): List<Token> {
        if (this.cachedTokens == null) {
            this.cachedTokens = this.lazyProvideTokens()
        }

        return this.cachedTokens!!
    }

    fun reset() {
        this.cachedTokens = null
    }
}

data class Token(
    private val weight: Double,
    val token: String,
    val compareStrategy: Applicable = FuzzyTokenMatcher.DEFAULT_STRATEGY,
    val exactMatchMultiplier: Double = DEFAULT_EXACT_MATCH_MULTIPLIER,
    val perfectRatioMultiplier: Double = DEFAULT_PERFECT_RATIO_MULTIPLIER
) {
    constructor(weight: Double, token: String) : this(
        weight,
        token,
        FuzzyTokenMatcher.SIMPLE_STRATEGY,
        DEFAULT_EXACT_MATCH_MULTIPLIER,
        DEFAULT_PERFECT_RATIO_MULTIPLIER
    )

    val tokenWeight = weight.coerceIn(0.0, 1.0)
}