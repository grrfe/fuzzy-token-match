package fe.fuzzytokenmatch

import me.xdrop.fuzzywuzzy.Applicable
import me.xdrop.fuzzywuzzy.algorithms.TokenSet
import me.xdrop.fuzzywuzzy.algorithms.TokenSort
import me.xdrop.fuzzywuzzy.algorithms.WeightedRatio
import me.xdrop.fuzzywuzzy.ratios.PartialRatio
import me.xdrop.fuzzywuzzy.ratios.SimpleRatio

class FuzzyTokenMatcher(
    private val minSum: Double = 90.0,
    private val compareStrategy: Applicable = DEFAULT_STRATEGY,
    private val exactMatchMultiplier: Double = DEFAULT_EXACT_MATCH_MULTIPLIER,
    private val perfectRatioMultiplier: Double = DEFAULT_PERFECT_RATIO_MULTIPLIER
) {
    companion object {
        @JvmField
        val SIMPLE_STRATEGY = SimpleRatio()

        @JvmField
        val PARTIAL_STRATEGY = PartialRatio()

        @JvmField
        val WEIGHTED_STRATEGY = WeightedRatio()

        @JvmField
        val TOKEN_SET_STRATEGY = TokenSet()

        @JvmField
        val TOKEN_SORT_STRATEGY = TokenSort()

        val DEFAULT_STRATEGY = SIMPLE_STRATEGY
        const val DEFAULT_EXACT_MATCH_MULTIPLIER = 1.0
        const val DEFAULT_PERFECT_RATIO_MULTIPLIER = 1.0

        private val COMPARATOR_MATCH_RESULT_DESC = Comparator.comparingDouble<MatchResult<*>> { it.ratioSum }
        private val COMPARATOR_MATCH_RESULT_ASC = COMPARATOR_MATCH_RESULT_DESC.reversed()
    }

    enum class SortType(val comparator: Comparator<MatchResult<*>>) {
        DESCENDING(COMPARATOR_MATCH_RESULT_ASC),
        ASCENDING(COMPARATOR_MATCH_RESULT_DESC);
    }

    fun <T : TokenProvider> match(
        query: String,
        providers: List<T>,
        providerMustMatchPredicate: ((T) -> Boolean)? = null,
        sortType: SortType = SortType.DESCENDING
    ): List<MatchResult<T>> {
        val searchQuery = query.trim()
        return providers.mapNotNull { provider ->
//            println("\tAnalyzing $provider")
            val ratioSum = provider.provideTokens().sumOf { token ->
                val exactMatchMultiplier =
                    if (token.exactMatchMultiplier != DEFAULT_EXACT_MATCH_MULTIPLIER) token.exactMatchMultiplier else this.exactMatchMultiplier
                val perfectRatioMultiplier =
                    if (token.perfectRatioMultiplier != DEFAULT_PERFECT_RATIO_MULTIPLIER) token.perfectRatioMultiplier else this.perfectRatioMultiplier


                if (searchQuery.equals(token.token, ignoreCase = true)) {
                    return@sumOf minSum * exactMatchMultiplier
                }

                val strategy = if (token.compareStrategy != DEFAULT_STRATEGY) token.compareStrategy else compareStrategy
                val ratio = with(strategy.apply(searchQuery.lowercase(), token.token.lowercase())) {
                    if (this == 100) this * perfectRatioMultiplier else this.toDouble()
                }

//                println("\t\tRatio for ${token.token}: $ratio, weighted: ${ratio * token.tokenWeight}")
                ratio * token.tokenWeight
            }

//            println("\t\t=$ratioSum (exceeds minSum: ${ratioSum >= minSum})")

            if (ratioSum >= minSum && providerMustMatchPredicate?.invoke(provider) == true) MatchResult(
                provider,
                ratioSum
            ) else null
        }.sortedWith(sortType.comparator)
    }
}

data class MatchResult<T : TokenProvider>(val tokenProvider: T, val ratioSum: Double)

