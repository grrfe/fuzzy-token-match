package fe.fuzzytokenmatch

fun main() {
//    val song1 = Song("Save Me", "Josh A")
//    val song2 = Song("Save Me", "7ru7h")
//    val song3 = Song("Save Me, Save You", "WJSN")
//    val song4 = Song("Joke's on you", "guccihighwaters")
//
    val list = listOf(
        Song("Primadonna", "Primadonna - Single", "Marina And The Diamonds", "Alternative", false),
        Song("Teen Idle", "Electra Heart (Deluxe Version)", "Marina And The Diamonds", "Pop"),
        Song("Hollywood", "The Family Jewels", "Marina And The Diamonds", "Alternative"),
        Song("Immortal", "FROOT", "Marina And The Diamonds", "Pop"),
        Song("Howling at the Moon", "Howling At the Moon - Single", "Milow", "Pop"),
        Song("Burning in the Skies", "A Thousand Suns", "Linkin Park", "Alternative"),
        Song("Monster", "Monster", "Nick Bonin", "Unknown Genre"),
        Song("Adriana", "Adriana", "RAF Camora", "Hip-Hop/Rap"),
        Song("Diamond", "The End of Nightmare", "Dreamcatcher", "KPOP")
    )


    val tokenMatcher = FuzzyTokenMatcher(exactMatchMultiplier = 20.0, perfectRatioMultiplier = 10.0)
//    matchTester(tokenMatcher, "Save Me", list)
//    matchTester(tokenMatcher, "Save Me Josh A", list)
//    matchTester(tokenMatcher, "Josh A Save Me", list)
//    matchTester(tokenMatcher, "Save You", list)
//    matchTester(tokenMatcher, "jokes on you", list)
//    matchTester(tokenMatcher, "gucci", list)
//    matchTester(tokenMatcher, "guccihigh", list)
//    matchTester(tokenMatcher, "guccihighwaters", list)
//
//    matchTester(tokenMatcher, "jokes", list)
//    matchTester(tokenMatcher, "jokes on", list)
//    matchTester(tokenMatcher, "save", list)
    matchTester(tokenMatcher, "Diamonds", list)


}

fun matchTester(tokenMatcher: FuzzyTokenMatcher, query: String, list: List<Song>) {
    println("Matching query '$query':")
    tokenMatcher.match(query, list, providerMustMatchPredicate = { it.test }).forEach { result ->
        println("\t${result.tokenProvider}: ${result.ratioSum}")
    }
}