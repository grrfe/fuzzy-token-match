package fe.fuzzytokenmatch

data class Song(val name: String, val artist: String, val album: String, val genre: String, val test: Boolean = true) :
    LazyTokenProvider() {
    override fun lazyProvideTokens(): List<Token> {
        TODO("Not yet implemented")
    }


    override fun provideTokens(): List<Token> {
        return listOf(
            Token(1.0, name, FuzzyTokenMatcher.PARTIAL_STRATEGY),
            Token(0.75, artist, FuzzyTokenMatcher.PARTIAL_STRATEGY),
            Token(0.75, album, FuzzyTokenMatcher.PARTIAL_STRATEGY),
            Token(0.2, genre)
        )
    }


}