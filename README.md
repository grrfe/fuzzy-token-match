# fuzzy-token-match

Provides a concise interface to match search queries against instances of classes which provide multiple weighted "tokens" as searchable properties. Based on [fuzzywuzzy](https://github.com/xdrop/fuzzywuzzy)
